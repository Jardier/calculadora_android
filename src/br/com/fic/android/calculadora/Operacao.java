package br.com.fic.android.calculadora;

public enum Operacao {
	ADDITION("Addition", "+"),
	SUBTRACTION("Subtraction", "-"),
	MULTIPLICATION("Multiplication", "X"),
	DIVISION("Division","/"),
	EQUAL("Equal", "=");

	private String descricao;
	private String simbolo;

	private Operacao(String descricao, String simbolo) {
		this.descricao = descricao;
		this.simbolo = simbolo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}
}
